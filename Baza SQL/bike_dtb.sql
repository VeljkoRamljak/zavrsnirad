-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 08, 2019 at 01:05 PM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bike_dtb`
--

-- --------------------------------------------------------

--
-- Table structure for table `biciklo`
--

DROP TABLE IF EXISTS `biciklo`;
CREATE TABLE IF NOT EXISTS `biciklo` (
  `ID_biciklo` int(10) NOT NULL AUTO_INCREMENT,
  `model` varchar(50) DEFAULT NULL,
  `velicina_guma` varchar(3) DEFAULT NULL,
  `broj_na_terminalu` int(10) DEFAULT NULL,
  `ID_stanica` int(10) DEFAULT NULL,
  PRIMARY KEY (`ID_biciklo`),
  UNIQUE KEY `ID_biciklo_2` (`ID_biciklo`),
  KEY `ID_biciklo` (`ID_biciklo`),
  KEY `ID_stanica` (`ID_stanica`)
) ENGINE=InnoDB AUTO_INCREMENT=10105 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `biciklo`
--

INSERT INTO `biciklo` (`ID_biciklo`, `model`, `velicina_guma`, `broj_na_terminalu`, `ID_stanica`) VALUES
(10103, 'brdski', '275', 1, 0),
(10104, 'romobil', '5', 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `korisnik`
--

DROP TABLE IF EXISTS `korisnik`;
CREATE TABLE IF NOT EXISTS `korisnik` (
  `ID_korisnik` int(10) NOT NULL AUTO_INCREMENT,
  `Ime_Prezime` varchar(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `lozinka` int(4) NOT NULL DEFAULT '1111',
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `aktivan` tinyint(1) NOT NULL DEFAULT '1',
  `OIB` bigint(13) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`ID_korisnik`),
  UNIQUE KEY `ID_korisnik` (`ID_korisnik`),
  UNIQUE KEY `OIB` (`OIB`),
  KEY `ID_korisnik_2` (`ID_korisnik`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `korisnik`
--

INSERT INTO `korisnik` (`ID_korisnik`, `Ime_Prezime`, `email`, `lozinka`, `admin`, `aktivan`, `OIB`) VALUES
(3, 'Veljko Ramljak', 'veljac95@gmail.com', 1111, 1, 1, 15963858174),
(4, 'Adam Opelović', 'Ado@crazymail.ba', 2222, 0, 1, 12345678910),
(5, 'roki', 'r@r', 1, 0, 1, 15963858144),
(6, 'Netko Netkić', 'mail@mail.com', 1, 0, 1, 15963858172),
(7, '20868073551', 'mai@gg', 11, 0, 1, 20868073551),
(8, 'dasds', 'dfdf@dfdf', 232, 0, 1, 31978414938),
(11, 'Tvrle BratMUU', 'tv@tv.com', 1, 0, 0, 81204382660),
(18, 'od brata bratic', 'br@br.com', 1, 0, 1, 40095070161);

-- --------------------------------------------------------

--
-- Table structure for table `stanica`
--

DROP TABLE IF EXISTS `stanica`;
CREATE TABLE IF NOT EXISTS `stanica` (
  `ID_stanica` int(10) NOT NULL AUTO_INCREMENT,
  `ulica` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID_stanica`),
  UNIQUE KEY `ID_terminal_2` (`ID_stanica`),
  KEY `ID_terminal` (`ID_stanica`),
  KEY `ID_stanica` (`ID_stanica`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stanica`
--

INSERT INTO `stanica` (`ID_stanica`, `ulica`) VALUES
(3, 'Posušje');

-- --------------------------------------------------------

--
-- Table structure for table `voznja`
--

DROP TABLE IF EXISTS `voznja`;
CREATE TABLE IF NOT EXISTS `voznja` (
  `ID_voznja` int(10) NOT NULL AUTO_INCREMENT,
  `vrijeme_pocetka` datetime NOT NULL,
  `vrijeme_zavrsetka` datetime DEFAULT NULL,
  `ID_stanica` int(10) NOT NULL,
  `ID_biciklo` int(10) NOT NULL,
  `ID_korisnik` int(10) NOT NULL,
  `aktivna` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID_voznja`),
  UNIQUE KEY `ID_voznja_2` (`ID_voznja`),
  KEY `ID_voznja` (`ID_voznja`),
  KEY `voznja_bic_ogr` (`ID_biciklo`),
  KEY `voznja_stanica_ogr` (`ID_stanica`),
  KEY `ID_korisnik` (`ID_korisnik`)
) ENGINE=InnoDB AUTO_INCREMENT=20251 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `voznja`
--

INSERT INTO `voznja` (`ID_voznja`, `vrijeme_pocetka`, `vrijeme_zavrsetka`, `ID_stanica`, `ID_biciklo`, `ID_korisnik`, `aktivna`) VALUES
(20236, '2019-09-05 20:09:48', '2019-09-05 20:10:15', 3, 10104, 5, 0),
(20237, '2019-09-05 20:13:06', '2019-09-05 20:13:19', 3, 10103, 5, 0),
(20238, '2019-09-05 20:15:06', '2019-09-05 20:15:41', 3, 10103, 5, 0),
(20239, '2019-09-05 20:23:10', '2019-09-05 20:23:34', 3, 10104, 5, 0),
(20240, '2019-09-05 20:23:49', '2019-09-05 20:24:37', 3, 10104, 5, 0),
(20241, '2019-09-05 20:24:17', '2019-09-06 16:29:51', 3, 10103, 3, 0),
(20242, '2019-09-06 16:29:36', '2019-09-06 16:30:12', 3, 10104, 5, 0),
(20243, '2019-09-06 17:14:07', '2019-09-06 17:14:24', 3, 10103, 5, 0),
(20244, '2019-09-06 17:14:51', '2019-09-06 17:21:01', 3, 10104, 5, 0),
(20245, '2019-09-06 17:15:26', '2019-09-06 17:21:20', 3, 10103, 3, 0),
(20246, '2019-09-06 17:18:29', '2019-09-06 17:20:34', 3, 10104, 4, 0),
(20247, '2019-09-06 18:11:03', '2019-09-07 15:06:41', 3, 10104, 5, 0),
(20248, '2019-09-07 15:07:17', '2019-09-07 15:07:52', 3, 10103, 5, 0),
(20249, '2019-09-08 13:24:08', '2019-09-08 13:24:19', 3, 10103, 5, 0),
(20250, '2019-09-08 13:24:31', NULL, 3, 10103, 5, 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `voznja`
--
ALTER TABLE `voznja`
  ADD CONSTRAINT `voznja_bic_ogr` FOREIGN KEY (`ID_biciklo`) REFERENCES `biciklo` (`ID_biciklo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `voznja_ibfk_1` FOREIGN KEY (`ID_korisnik`) REFERENCES `korisnik` (`ID_korisnik`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `voznja_stanica_ogr` FOREIGN KEY (`ID_stanica`) REFERENCES `stanica` (`ID_stanica`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
