﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BikeSharing
{
    public partial class NoviTerminal : Form
    {
        public NoviTerminal()
        {
            InitializeComponent();
        }
        //INSERT
        private void button1_Click(object sender, EventArgs e)
        {
            //provjera jesu li upisani svi podaci
            if (string.IsNullOrEmpty(tbUlica.Text))
            {
                //ako nisu svi upisani
                MessageBox.Show("Molimo unesite naziv ulice! ");
            }
            else //ako su svi upisani
            {
                    //Izvedi sql tako da se povlače podaci iz text boxsova
                    string sqlInsta = "INSERT INTO `stanica`  (`ID_stanica`, `ulica`) " +
                        " VALUES (NULL,'" + tbUlica.Text + "') ";
                    MessageBox.Show(sqlInsta);
                    BazaKontroler.InsertRow(sqlInsta);

                    //Uspjesno dodana nova stanica
                    MessageBox.Show("Uspjesno dodana nova stanica");
                    //refreš tablice
                    String sql = "SELECT * FROM `stanica` WHERE 1 ";
                    dgvStanica.DataSource = BazaKontroler.IvediSqlVratiDT(sql);
                

            }
        }
        //UPDATE
        private void btUpdate_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbUlica.Text))
            {
                //ako nisu svi upisani
                MessageBox.Show("Molimo unesite naziv ulice! ");
            }
            else //ako su svi upisani
            {
                //ako su validno upisani
                //Izvedi sql update tako da se povlače podaci iz text boxsova
                string sqlUpda = "UPDATE `stanica` SET" +
                    " `ulica`='" + tbUlica.Text + "' " +
                    " WHERE `ID_stanica`like " + StanicaSesija.stanSesZaDelete.GetId_stanica() + "";
                 MessageBox.Show(sqlUpda);
                BazaKontroler.Update(sqlUpda);

                //Uspjesno izmjenjeno  biciklo
                MessageBox.Show("Uspjesno izmjenjen naziv ulice");
                //refreš tablice
                String sql = "SELECT * FROM `stanica` WHERE 1 ";
                dgvStanica.DataSource = BazaKontroler.IvediSqlVratiDT(sql);
            }

        }
        //DELETE
        private void btDelete_Click(object sender, EventArgs e)
        {
            //Izbaciti prozor sigurnosti da li se ste sigurni
            DialogResult diaR = MessageBox.Show("Jeste li sigurni da želite izbrisati stanicu? Sa stanicom su povezana bicikla i vožnje",
                      "Upozorenje!!", MessageBoxButtons.YesNo);
            switch (diaR)
            {
                case DialogResult.Yes:
                    //DOKVATITI ID_stanica od kliknute stanice
                    string sqlDelete = "DELETE FROM `stanica` WHERE `ID_stanica` = " + StanicaSesija.stanSesZaDelete.GetId_stanica() + "";

                    MessageBox.Show(sqlDelete);

                    BazaKontroler.Delete(sqlDelete);

                    //Trajno uklonjen korisnik
                    MessageBox.Show("Trajno uklonjena stanica: " + StanicaSesija.stanSesZaDelete.GetUlica());
                    //refreš tablice
                    String sql = "SELECT * FROM `stanica` WHERE 1 ";
                    dgvStanica.DataSource = BazaKontroler.IvediSqlVratiDT(sql);
                    break;
                case DialogResult.No:
                    break;
            }
        }
        //ON click
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int index = e.RowIndex;
                DataGridViewRow odabraniRed = dgvStanica.Rows[index];

                StanicaSesija.stanSesZaDelete.SetId_stanica(Int32.Parse(odabraniRed.Cells["ID_stanica"].Value.ToString()));
                StanicaSesija.stanSesZaDelete.SetUlica(odabraniRed.Cells["ulica"].Value.ToString());

                tbUlica.Text = StanicaSesija.stanSesZaDelete.GetUlica();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        //ON LOAD
        private void NoviTerminal_Load(object sender, EventArgs e)
        {
            String sql = "SELECT * FROM `stanica` WHERE 1 ";
            dgvStanica.DataSource = BazaKontroler.IvediSqlVratiDT(sql);

            //try>> ukoliko se ne odabere stanica u odabir ulazi prva s reda :)>>>
            try
            {

                int index = dgvStanica.CurrentCell.RowIndex;
                DataGridViewRow odabraniRed = dgvStanica.Rows[index];

                StanicaSesija.stanSesZaDelete.SetId_stanica(Int32.Parse(odabraniRed.Cells["ID_stanica"].Value.ToString()));
                StanicaSesija.stanSesZaDelete.SetUlica(odabraniRed.Cells["ulica"].Value.ToString());

                tbUlica.Text = StanicaSesija.stanSesZaDelete.GetUlica();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
