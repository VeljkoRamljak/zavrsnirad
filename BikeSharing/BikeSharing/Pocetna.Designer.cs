﻿namespace BikeSharing
{
    partial class Pocetna
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button2 = new System.Windows.Forms.Button();
            this.btMojeVoznje = new System.Windows.Forms.Button();
            this.chBiciklo = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chBicikloPita = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chBiciklo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chBicikloPita)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(392, 444);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(163, 75);
            this.button1.TabIndex = 0;
            this.button1.Text = "Iznajmi";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(543, 426);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(962, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(92, 30);
            this.button2.TabIndex = 2;
            this.button2.Text = "odjava";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btMojeVoznje
            // 
            this.btMojeVoznje.Location = new System.Drawing.Point(846, 12);
            this.btMojeVoznje.Name = "btMojeVoznje";
            this.btMojeVoznje.Size = new System.Drawing.Size(92, 30);
            this.btMojeVoznje.TabIndex = 3;
            this.btMojeVoznje.Text = "moje vožnje";
            this.btMojeVoznje.UseVisualStyleBackColor = true;
            this.btMojeVoznje.Click += new System.EventHandler(this.btMojeVoznje_Click);
            // 
            // chBiciklo
            // 
            chartArea1.Name = "ChartArea1";
            this.chBiciklo.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chBiciklo.Legends.Add(legend1);
            this.chBiciklo.Location = new System.Drawing.Point(616, 69);
            this.chBiciklo.Name = "chBiciklo";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chBiciklo.Series.Add(series1);
            this.chBiciklo.Size = new System.Drawing.Size(381, 210);
            this.chBiciklo.TabIndex = 4;
            this.chBiciklo.Text = "bicikla";
            // 
            // chBicikloPita
            // 
            chartArea2.Name = "ChartArea1";
            this.chBicikloPita.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chBicikloPita.Legends.Add(legend2);
            this.chBicikloPita.Location = new System.Drawing.Point(616, 285);
            this.chBicikloPita.Name = "chBicikloPita";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chBicikloPita.Series.Add(series2);
            this.chBicikloPita.Size = new System.Drawing.Size(381, 214);
            this.chBicikloPita.TabIndex = 5;
            this.chBicikloPita.Text = "chart1";
            // 
            // Pocetna
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1066, 533);
            this.Controls.Add(this.chBicikloPita);
            this.Controls.Add(this.chBiciklo);
            this.Controls.Add(this.btMojeVoznje);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button1);
            this.Name = "Pocetna";
            this.Text = "Dobrodošli";
            this.Load += new System.EventHandler(this.Pocetna_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chBiciklo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chBicikloPita)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btMojeVoznje;
        private System.Windows.Forms.DataVisualization.Charting.Chart chBiciklo;
        private System.Windows.Forms.DataVisualization.Charting.Chart chBicikloPita;
    }
}