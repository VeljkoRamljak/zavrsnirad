﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BikeSharing
{
    class BazaKontroler
    {
        //Singelton
        public static BazaKontroler bazaKontrolerInstanca;

        //sadas

        
        public static string connstr = "DRIVER={MySQL ODBC 3.51 Driver};SERVER=localhost;PORT=3306;DATABASE=bike_dtb; USER ID=root;PASSWORD=;";
        public static OdbcConnection konekcija=new OdbcConnection(connstr);
       
        public string  SQL { get; set; }
       
        //public static DataGridView dataGridView2 = new DataGridView();



        public BazaKontroler()
        {
            
            if (bazaKontrolerInstanca != null)
                return;
            else
                bazaKontrolerInstanca = this;
        }
        //treba li static????
        public static void Otvori()
        {

            try
            {
                if (!(konekcija.State == ConnectionState.Closed))
                {
                    BazaKontroler.Zatvori();
                }
                BazaKontroler.konekcija.Open();
                MessageBox.Show("otvoreno jeee");

                
            }
            catch
            {
                MessageBox.Show("Došlo je do greške u vezi!.\n Provjerite vezu s internetom!", "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static void Zatvori()
        {

            try
            {
              
                BazaKontroler.konekcija.Close();
            }
            catch
            {
                MessageBox.Show("Došlo je do greške u vezi!.\n Provjerite vezu s internetom!", "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public static OdbcDataReader IzvediSQL(string sql)
        {
            
            try
            {
                if (sql == null)
                return null;
                BazaKontroler.Otvori();
                OdbcCommand cmd = new OdbcCommand(sql, BazaKontroler.konekcija);
                OdbcDataReader dr = cmd.ExecuteReader();
                return dr;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

                return null;
            }
                       
            
        }
        public static DataTable IvediSqlVratiDT(string sql)
        {
            if (sql == null)
                return null;

            try
            {
                DataTable dt = new DataTable();
                dt.Load(BazaKontroler.IzvediSQL(sql));
                BazaKontroler.Zatvori();
                return dt;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Mololimo odaberite red");
                MessageBox.Show(ex.Message);
                    return null;
            }
        }
        public static void InsertRow( string insertSQL)
        {
            
                // The insertSQL string contains a SQL statement that
                // inserts a new row in the source table.
                OdbcCommand command = new OdbcCommand(insertSQL, BazaKontroler.konekcija);

                // Open the connection and execute the insert command.
                try
                {
                    BazaKontroler.Otvori();
                    int n = command.ExecuteNonQuery();
                if (n == 0)
                {
                    MessageBox.Show("Insert nije prošao!    ako je kod korisnika OIB je zauzet.");
                }
                        
                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                // The connection is automatically closed when the
                // code exits the using block.
        }
        public static void Update(string updateSQL)
        {

                //Upate non query
                OdbcCommand command = new OdbcCommand(updateSQL, BazaKontroler.konekcija);

                // Open the connection and execute the insert command.
                try
                {
                    BazaKontroler.Otvori();
                    command.ExecuteNonQuery();
                
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                // The connection is automatically closed when the
                // code exits the using block.
        }
        public static void Delete(string deleteSQL)
        {

            //Upate non query
            OdbcCommand command = new OdbcCommand(deleteSQL, BazaKontroler.konekcija);

            // Open the connection and execute the insert command.
            try
            {
                BazaKontroler.Otvori();
                int n = command.ExecuteNonQuery();
                if (n == 0)
                {
                    MessageBox.Show("delete nije prošao!  ");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            // The connection is automatically closed when the
            // code exits the using block.
        }
    }

}


         
   


