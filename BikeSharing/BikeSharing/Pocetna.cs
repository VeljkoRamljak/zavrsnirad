﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Windows.Forms.DataVisualization.Charting;
using System.Data.Odbc;

namespace BikeSharing
{
    public partial class Pocetna : Form
    {
        public Pocetna()
        {
            InitializeComponent();
        }

        private void Pocetna_Load(object sender, EventArgs e)
        {
            String sql = "SELECT * FROM `biciklo` WHERE `ID_stanica` NOT LIKE 0 AND `ID_stanica` LIKE "+StanicaSesija.stanSes.GetId_stanica()+" ";
            dataGridView1.DataSource = BazaKontroler.IvediSqlVratiDT(sql);

            //try>> ukoliko se ne odabere biciklo u odabir ulazi prvo s reda :)>>>
            try
            {
               
                int index = dataGridView1.CurrentCell.RowIndex;
                DataGridViewRow odabraniRed = dataGridView1.Rows[index];

                BikeSes.bikeSes.SetId_biciklo(Int32.Parse(odabraniRed.Cells["id_biciklo"].Value.ToString()));
                BikeSes.bikeSes.SetVelicina_guma(odabraniRed.Cells["velicina_guma"].Value.ToString());
                BikeSes.bikeSes.SetBroj_na_terminalu(Int32.Parse(odabraniRed.Cells["broj_na_terminalu"].Value.ToString()));
                BikeSes.bikeSes.SetId_stanica(Int32.Parse(odabraniRed.Cells["id_stanica"].Value.ToString()));
                BikeSes.bikeSes.SetModel(odabraniRed.Cells["model"].Value.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            //CHART statistika
            this.chBiciklo.Series.Clear();
            this.chBicikloPita.Series.Clear();
            //Naslov charta
            //this.chart1.Titles.Add("Naslov");

            Series series = this.chBiciklo.Series.Add("Broj Vožnji");
            Series seriess = this.chBicikloPita.Series.Add("Broj Vožnji");
            series.ChartType = SeriesChartType.Column;
            seriess.ChartType = SeriesChartType.Pie;
            //series.Points.AddXY("ImeKorisnika", brojIznajmljivanja);
            //series.Points.AddXY("Obtober", 300);

            string sqll = "SELECT DISTINCT(biciklo.model), COUNT(*) AS Broj FROM voznja " +
                " Join biciklo ON biciklo.ID_biciklo = voznja.ID_biciklo " +
                "WHERE `voznja`.`ID_korisnik` LIKE "+KorisnikSesija.korSes.GetId_korisnik()+" " +
                " GROUP BY biciklo.model";
            OdbcDataReader dataR = BazaKontroler.IzvediSQL(sqll);
            while (dataR.Read())
            {
                series.Points.AddXY(dataR["model"], dataR["Broj"]);
                seriess.Points.AddXY(dataR["model"], dataR["Broj"]);

            }
            BazaKontroler.Zatvori();

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string sqlInsert= "INSERT INTO `voznja` (`ID_voznja`, `vrijeme_pocetka`, `vrijeme_zavrsetka`, `ID_stanica`, `ID_biciklo`, `ID_korisnik`, `aktivna`)" +
                " VALUES (NULL, CURRENT_TIME(), NULL, '"+StanicaSesija.stanSes.GetId_stanica()+ "', '" + BikeSes.bikeSes.GetId_biciklo() + "', '" + KorisnikSesija.korSes.GetId_korisnik() + "', '1')";
            MessageBox.Show(sqlInsert);
            BazaKontroler.InsertRow(sqlInsert);

            MessageBox.Show(KorisnikSesija.korSes.GetIme_prezime() + ": uspješno ste iznajmili biciklo " + BikeSes.bikeSes.GetModel());

            //UPDATE BICIKLO SKINUTI ID_stanice U BAZI
            string sqlUpdate = "UPDATE `biciklo` SET `ID_stanica` = 0 WHERE `ID_biciklo` like "+BikeSes.bikeSes.GetId_biciklo()+";";
            BazaKontroler.Update(sqlUpdate);

            KorisnikSesija.PoništiSes();
            BikeSes.PoništiSes();
            Prijava poc = new Prijava(); // Instantiate a Form3 object.
            poc.Show(); // Show Form3 and
            this.Close();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int index = e.RowIndex;
                DataGridViewRow odabraniRed = dataGridView1.Rows[index];
                
                BikeSes.bikeSes.SetId_biciklo(Int32.Parse(odabraniRed.Cells["id_biciklo"].Value.ToString()));
                BikeSes.bikeSes.SetVelicina_guma(odabraniRed.Cells["velicina_guma"].Value.ToString());
                BikeSes.bikeSes.SetBroj_na_terminalu(Int32.Parse(odabraniRed.Cells["broj_na_terminalu"].Value.ToString()));
                BikeSes.bikeSes.SetId_stanica(Int32.Parse(odabraniRed.Cells["id_stanica"].Value.ToString()));
                BikeSes.bikeSes.SetModel(odabraniRed.Cells["model"].Value.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            VoznjaSes.Poništi();
            BikeSes.PoništiSes();
            KorisnikSesija.PoništiSes();

            Prijava poc = new Prijava(); // Instantiate a Form3 object.
            poc.Show(); // Show Form3 and
            this.Close();
        }

        private void btMojeVoznje_Click(object sender, EventArgs e)
        {
            KorisnikVoznje korVoz = new KorisnikVoznje(); // Instantiate a Form3 object.
            korVoz.Show(); // Show Form3 and
        }
    }
}
