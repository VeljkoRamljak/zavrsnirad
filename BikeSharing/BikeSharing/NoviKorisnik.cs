﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BikeSharing
{
    public partial class NoviKorisnik : Form
    {
         Korisnik KorisnikZaDelete = new Korisnik();
        public NoviKorisnik()
        {
            InitializeComponent();

        }
        //INSERT tipka
        private void button1_Click(object sender, EventArgs e)
        {
            //provjera jesu li upisani svi podaci
            if (string.IsNullOrEmpty(tbAdmin.Text) || string.IsNullOrEmpty(tbAktivan.Text) 
                || string.IsNullOrEmpty(tbEmail.Text) || string.IsNullOrEmpty(tbImePrezime.Text)
                || string.IsNullOrEmpty(tbLozinka.Text) || string.IsNullOrEmpty(tbOIB.Text))
            {
                //ako nisu svi upisani
               MessageBox.Show("Molimo unesite sve podatke! ");
            }
            else //ako su svi upisani
            {
                //provjera jesu li ispravno(validno) upisani podaci
                int lozaProvjera;
                bool adminProvjera= false;
                bool aktivanProvjera = false;
                if (!(Kontrola.IsValidEmail(tbEmail.Text)
                    && Int32.TryParse(tbLozinka.Text,out lozaProvjera) 
                    && !bool.TryParse(tbAdmin.Text,out adminProvjera)
                    && !bool.TryParse(tbAktivan.Text, out aktivanProvjera) 
                    && Kontrola.CheckOIB(tbOIB.Text)
                    && !Kontrola.ProvjeraPostoliLiEmailUBazi(tbEmail.Text)))
                {
                    //ako nisu validno upisani
                    MessageBox.Show("Niste ispravno(validno) unijeli podatke!  oib je:" + Kontrola.CheckOIB(tbOIB.Text).ToString()+" email postoji u bazi:"+ Kontrola.ProvjeraPostoliLiEmailUBazi(tbEmail.Text).ToString());
                }
                else
                {   //ako su validno upisani
                    //Izvedi sql tako da se povlače podaci iz text boxsova
                    string sqlInserrt = "INSERT INTO `korisnik` (`ID_korisnik`, `Ime_Prezime`, `email`, `lozinka`, `admin`, `aktivan`, `OIB`) " +
                        " VALUES (NULL, '"+tbImePrezime.Text+"', '"+tbEmail.Text+"', '"+tbLozinka.Text+"', '"+tbAdmin.Text+"', '"+tbAktivan.Text+"', '"+tbOIB.Text+"');";
                    MessageBox.Show(sqlInserrt);
                    BazaKontroler.InsertRow(sqlInserrt);

                    //Uspjesno dodan novi korisnik
                    MessageBox.Show("Uspjesno dodan novi korisnik");
                    //refreš tablice
                    String sql = "SELECT * FROM `korisnik` WHERE 1 ";
                    dgvKorisnik.DataSource = BazaKontroler.IvediSqlVratiDT(sql);
                }
            

               
            }
             
            
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            String sql = "SELECT * FROM `korisnik` WHERE 1 ";
            dgvKorisnik.DataSource = BazaKontroler.IvediSqlVratiDT(sql);

            //try>> ukoliko se ne odabere korisnik u odabir ulazi prvo s reda :)>>>
            try
            {

               /* int index = dgvKorisnik.CurrentCell.RowIndex;
                DataGridViewRow odabraniRed = dgvKorisnik.Rows[index];

                BikeSes.bikeSes.SetId_biciklo(Int32.Parse(odabraniRed.Cells["id_biciklo"].Value.ToString()));
                BikeSes.bikeSes.SetVelicina_guma(odabraniRed.Cells["velicina_guma"].Value.ToString());
                BikeSes.bikeSes.SetBroj_na_terminalu(Int32.Parse(odabraniRed.Cells["broj_na_terminalu"].Value.ToString()));
                BikeSes.bikeSes.SetId_stanica(Int32.Parse(odabraniRed.Cells["id_stanica"].Value.ToString()));
                BikeSes.bikeSes.SetModel(odabraniRed.Cells["model"].Value.ToString());
                */
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dgvKorisnik_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int index = e.RowIndex;
                DataGridViewRow odabraniRed = dgvKorisnik.Rows[index];
                string sql11 = "SELECT * FROM `korisnik` WHERE `ID_korisnik` LIKE " + odabraniRed.Cells["ID_korisnik"].Value.ToString();
               
                //MessageBox.Show(sql11);
                KorisnikZaDelete = Korisnik.DohvatiKorisnikaIzBaze(BazaKontroler.IzvediSQL(sql11));
                KorisnikSesija.korisnikZaPregled = KorisnikZaDelete;

                tbImePrezime.Text = KorisnikZaDelete.GetIme_prezime();
                tbEmail.Text = KorisnikZaDelete.GetEmail();
                tbLozinka.Text = KorisnikZaDelete.GetLozinka();
                int admin =  KorisnikZaDelete.GetAdmin() ? 1 : 0;
                    tbAdmin.Text = admin.ToString();
                int aktivan = KorisnikZaDelete.GetAktivan() ? 1 : 0;
                    tbAktivan.Text = aktivan.ToString();
                tbOIB.Text = KorisnikZaDelete.GetOIB();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
        //UPDATE tipka
        private void button1_Click_1(object sender, EventArgs e)
        {
            //prepisati provjeru unosa od inserta

            //provjera jesu li upisani svi podaci
            if (string.IsNullOrEmpty(tbAdmin.Text) || string.IsNullOrEmpty(tbAktivan.Text)
                || string.IsNullOrEmpty(tbEmail.Text) || string.IsNullOrEmpty(tbImePrezime.Text)
                || string.IsNullOrEmpty(tbLozinka.Text) || string.IsNullOrEmpty(tbOIB.Text))
            {
                //ako nisu svi upisani
                MessageBox.Show("update Molimo unesite sve podatke! ");
            }
            else //ako su svi upisani
            {
                //provjera jesu li ispravno(validno) upisani podaci
                int lozaProvjera;
                bool adminProvjera = false;
                bool aktivanProvjera = false;
                if (!(Kontrola.IsValidEmail(tbEmail.Text)
                    && Int32.TryParse(tbLozinka.Text, out lozaProvjera)
                    && !bool.TryParse(tbAdmin.Text, out adminProvjera)
                    && !bool.TryParse(tbAktivan.Text, out aktivanProvjera)
                    && Kontrola.CheckOIB(tbOIB.Text)))
                {
                    //ako nisu validno upisani
                    MessageBox.Show("update Niste ispravno(validno) unijeli podatke!  oib je:" + Kontrola.CheckOIB(tbOIB.Text).ToString() );
                }
                else
                {   //ako su validno upisani
                    //Izvedi update sql tako da se povlače podaci iz text boxsova

                    if (tbEmail.Text.Equals(KorisnikZaDelete.GetEmail()))
                    {
                        MessageBox.Show("Email se ne može promijeniti!!");
                    }
                   
                    string sqlUpdate = "UPDATE `korisnik`" +
                        " SET `Ime_Prezime`='" + tbImePrezime.Text + "'," +
                        "`lozinka`='" + tbLozinka.Text + "',`admin`='" + tbAdmin.Text + "'," +
                        "`aktivan`='" + tbAktivan.Text + "',`OIB`='" + tbOIB.Text + "' WHERE `ID_korisnik` like '" + KorisnikZaDelete.GetId_korisnik() + "'";
                    
                    MessageBox.Show(sqlUpdate);
                    BazaKontroler.Update(sqlUpdate);

                    //Uspjesno izmjenjen korisnik
                    MessageBox.Show("update Uspjesno izmjenjen novi korisnik");
                    //refreš tablice
                    String sql = "SELECT * FROM `korisnik` WHERE 1 ";
                    dgvKorisnik.DataSource = BazaKontroler.IvediSqlVratiDT(sql);
                }
            }
        }

        //DELETE tipka
        private void btDelete_Click(object sender, EventArgs e)
        {
            //Izbaciti prozor sigurnosti da li se ste sigurni
            DialogResult diaR = MessageBox.Show("Jeste li sigurni da želite izbrisati korisnika?",
                      "Upozorenje!!", MessageBoxButtons.YesNo);
            switch (diaR)
            {
                case DialogResult.Yes:
                    //DOKVATITI ID_korisnik od kliknutog korisnika
                    string sqlDelete = "DELETE FROM `korisnik` WHERE `ID_korisnik` = "+KorisnikZaDelete.GetId_korisnik()+"";

                    MessageBox.Show(sqlDelete);

                    BazaKontroler.Delete(sqlDelete);

                    //Trajno uklonjen korisnik
                    MessageBox.Show("Trajno uklonjen korisnik: "+KorisnikZaDelete.GetIme_prezime());
                    //refreš tablice
                    String sql = "SELECT * FROM `korisnik` WHERE 1 ";
                    dgvKorisnik.DataSource = BazaKontroler.IvediSqlVratiDT(sql);
                    break;
                case DialogResult.No:
                    break;
            }
            
        }

        private void btPregledajKorisnika_Click(object sender, EventArgs e)
        {
            PregledKorisnika prefKor = new PregledKorisnika();
            prefKor.Show();
        }
    }
}
