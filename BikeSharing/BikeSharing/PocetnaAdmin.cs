﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Odbc;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace BikeSharing
{
    public partial class PocetnaAdmin : Form
    {
        public PocetnaAdmin()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string skjuL = "SELECT `ulica` ,`ID_Stanica` FROM `stanica`";
            OdbcDataReader drd = BazaKontroler.IzvediSQL(skjuL);
            try
            {
                while (drd.Read())
                {
                    comboBox1.Items.Add(drd["ID_stanica"].ToString());
                    //comboBox1.ValueMember = drd["ID_stanica"].ToString();
                    //comboBox1.DisplayMember = drd["ulica"].ToString();
                }
                comboBox1.Text = StanicaSesija.stanSes.GetId_stanica().ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            //DOKVATIT TRENUTNE VOŽNJE
            String sql = "SELECT * FROM `voznja` WHERE `aktivna` LIKE 1";
            dGVAdmin.DataSource = BazaKontroler.IvediSqlVratiDT(sql);

            //POKUŠATI STATISTIKU UBACITI

            //učitati chart statistiku
            button1_Click(sender, e);            


        }
        

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            NoviKorisnik f4 = new NoviKorisnik(); // Instantiate a Form3 object.
            f4.Show(); // Show Form3 and
        }

        private void modeli_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            NoviTerminal fterm = new NoviTerminal(); // Instantiate a Form3 object.
            fterm.Show(); // Show Form3 and
        }

        private void toolStripMenuItem7_Click(object sender, EventArgs e)
        {
            NovoBiciklo fbiciklo = new NovoBiciklo(); // Instantiate a Form3 object.
            fbiciklo.Show(); // Show Form3 and
        }

        private void dataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void butOdjava_Click(object sender, EventArgs e)
        {
            VoznjaSes.Poništi();
            BikeSes.PoništiSes();
            KorisnikSesija.PoništiSes();

            Prijava poc = new Prijava(); // Instantiate a Form3 object.
            poc.Show(); // Show Form3 and
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            this.chart1.Series.Clear();
            //Naslov charta
            //this.chart1.Titles.Add("Naslov");

            Series series = this.chart1.Series.Add("Broj Vožnji");
            series.ChartType = SeriesChartType.Column;
            //series.Points.AddXY("ImeKorisnika", brojIznajmljivanja);
            //series.Points.AddXY("Obtober", 300);
            
            string sql = "SELECT DISTINCT(korisnik.Ime_Prezime), COUNT(*) AS Broj FROM voznja " +
                " Join korisnik ON korisnik.ID_korisnik = voznja.ID_korisnik " +
                " GROUP BY korisnik.Ime_Prezime";
            OdbcDataReader dataR = BazaKontroler.IzvediSQL(sql);
                while (dataR.Read())
                {
                    series.Points.AddXY(dataR["Ime_Prezime"], dataR["Broj"]);
                    
                }
                BazaKontroler.Zatvori();

            this.chBikePita.Series.Clear();
            //Naslov charta
            //this.chart1.Titles.Add("Naslov");

            Series seriees = this.chBikePita.Series.Add("Bicikla frekvencija");
            seriees.ChartType = SeriesChartType.Column;
            //series.Points.AddXY("ImeKorisnika", brojIznajmljivanja);
            //series.Points.AddXY("Obtober", 300);

            string sqll = "SELECT DISTINCT(biciklo.model), COUNT(*) AS Broj FROM voznja " +
                " Join biciklo ON biciklo.ID_biciklo= voznja.ID_biciklo " +
                " GROUP BY biciklo.model";
            OdbcDataReader dataRe = BazaKontroler.IzvediSQL(sqll);
            while (dataRe.Read())
            {
                seriees.Points.AddXY(dataRe["model"], dataRe["Broj"]);
                

            }
            BazaKontroler.Zatvori();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            StanicaSesija.stanSes.SetId_stanica(Int32.Parse(comboBox1.SelectedItem.ToString()));
            MessageBox.Show(comboBox1.SelectedItem.ToString());
        }
    }
}
