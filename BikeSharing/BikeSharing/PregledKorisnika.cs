﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Odbc;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace BikeSharing
{
    public partial class PregledKorisnika : Form
    {
        public PregledKorisnika()
        {
            InitializeComponent();
        }

        private void PregledKorisnika_Load(object sender, EventArgs e)
        {
            //ucitavanje voznji
            String sql = "SELECT * FROM `voznja` WHERE `ID_korisnik` LIKE " + KorisnikSesija.korisnikZaPregled.GetId_korisnik();
            dgvPregledKorsinika.DataSource = BazaKontroler.IvediSqlVratiDT(sql);

            //CHART statistika
            this.chart1.Series.Clear();
            this.chart2.Series.Clear();
            //Naslov charta
            //this.chart1.Titles.Add("Naslov");

            Series series = this.chart1.Series.Add("Broj Vožnji");
            Series seriess = this.chart2.Series.Add("Broj Vožnji");
            series.ChartType = SeriesChartType.Column;
            seriess.ChartType = SeriesChartType.Pie;
            //series.Points.AddXY("ImeKorisnika", brojIznajmljivanja);
            //series.Points.AddXY("Obtober", 300);

            string sqll = "SELECT DISTINCT(biciklo.model), COUNT(*) AS Broj FROM voznja " +
                " Join biciklo ON biciklo.ID_biciklo = voznja.ID_biciklo " +
                "WHERE `voznja`.`ID_korisnik` LIKE " + KorisnikSesija.korisnikZaPregled.GetId_korisnik() + " " +
                " GROUP BY biciklo.model";
            OdbcDataReader dataR = BazaKontroler.IzvediSQL(sqll);
            try
            {
                while (dataR.Read())
                {
                    series.Points.AddXY(dataR["model"], dataR["Broj"]);
                    seriess.Points.AddXY(dataR["model"], dataR["Broj"]);

                }
                BazaKontroler.Zatvori();
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
