﻿namespace BikeSharing
{
    partial class KorisnikVoznje
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvKorisnikVoznje = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKorisnikVoznje)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvKorisnikVoznje
            // 
            this.dgvKorisnikVoznje.AllowUserToAddRows = false;
            this.dgvKorisnikVoznje.AllowUserToDeleteRows = false;
            this.dgvKorisnikVoznje.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvKorisnikVoznje.Location = new System.Drawing.Point(13, 13);
            this.dgvKorisnikVoznje.Name = "dgvKorisnikVoznje";
            this.dgvKorisnikVoznje.ReadOnly = true;
            this.dgvKorisnikVoznje.Size = new System.Drawing.Size(709, 580);
            this.dgvKorisnikVoznje.TabIndex = 0;
            // 
            // KorisnikVoznje
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 605);
            this.Controls.Add(this.dgvKorisnikVoznje);
            this.Name = "KorisnikVoznje";
            this.Text = "Moje Vožnje";
            this.Load += new System.EventHandler(this.KorisnikVoznje_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvKorisnikVoznje)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvKorisnikVoznje;
    }
}