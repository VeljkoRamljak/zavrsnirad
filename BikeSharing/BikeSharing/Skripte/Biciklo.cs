﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace BikeSharing
{
    class Biciklo
    {
        private int id_biciklo=0;
        private int id_stanica=0;
        private String model;
        private String velicina_guma;
        private int broj_na_terminalu;
        
        
        public int GetId_biciklo()
        {
            return id_biciklo;
        }

        public void SetId_biciklo(int value)
        {
            id_biciklo = value;
        }

        public string GetModel()
        {
            return model;
        }

        public void SetModel(string value)
        {
            model = value;
        }

        public string GetVelicina_guma()
        {
            return velicina_guma;
        }

        public void SetVelicina_guma(string value)
        {
            velicina_guma = value;
        }

        public int GetBroj_na_terminalu()
        {
            return broj_na_terminalu;
        }

        public void SetBroj_na_terminalu(int value)
        {
            broj_na_terminalu = value;
        }

        private int iD_stanica;

        public int GetId_stanica()
        {
            return id_stanica;
        }

        public void SetId_stanica(int value)
        {
            id_stanica = value;
        }

        public int GetID_stanica()
        {
            return iD_stanica;
        }

        public void SetID_stanica(int value)
        {
            iD_stanica = value;
        }

        public static DataTable DohvatiSveBicikle()
        {
            DataTable tablica=new DataTable();
            tablica.TableName = "Bicikla";

            
            return tablica;
        }
    }

}
