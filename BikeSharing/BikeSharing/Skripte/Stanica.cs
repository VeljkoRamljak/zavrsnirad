﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BikeSharing
{
    class Stanica
    {
        private int id_stanica;
        private String ulica;

        public int GetId_stanica()
        {
            return id_stanica;
        }

        public void SetId_stanica(int value)
        {
            id_stanica = value;
        }

        public string GetUlica()
        {
            return ulica;
        }

        public void SetUlica(string value)
        {
            ulica = value;
        }
    }
}
