﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BikeSharing
{
    class Korisnik
    {
        private String id_korisnik;
        private String ime_prezime;
        private String email;
        private String lozinka;
        private String OIB;
        private bool admin;
        private bool aktivan;

        public string GetOIB()
        {
            return OIB;
        }

        public void SetOIB(string value)
        {
            OIB = value;
        }

        public string GetEmail()
        {
            return email;
        }

        public void SetEmail(string value)
        {
            email = value;
        }

        public string GetId_korisnik()
        {
            return id_korisnik;
        }

        public void SetId_korisnik(string value)
        {
            id_korisnik = value;
        }

        public string GetIme_prezime()
        {
            return ime_prezime;
        }

        public void SetIme_prezime(string value)
        {
            ime_prezime = value;
        }

        public string GetLozinka()
        {
            return lozinka;
        }

        public void SetLozinka(string value)
        {
            lozinka = value;
        }

        public bool GetAdmin()
        {
            return admin;
        }

        public void SetAdmin(bool value)
        {
            admin = value;
        }

        public bool GetAktivan()
        {
            return aktivan;
        }

        public void SetAktivan(bool value)
        {
            aktivan = value;
        }

        public static Korisnik DohvatiKorisnikaIzBaze(OdbcDataReader dr)
        {
            Korisnik korisnik = new Korisnik();

            if (dr != null && dr.HasRows == true)
            {
                while (dr.Read())
                {
                    korisnik.SetId_korisnik(dr["ID_korisnik"].ToString());
                    korisnik.SetIme_prezime(dr["Ime_Prezime"].ToString());
                    korisnik.SetEmail(dr["Email"].ToString());
                    korisnik.SetAdmin(Convert.ToBoolean(dr["Admin"]));
                    korisnik.SetAktivan(Convert.ToBoolean(dr["Aktivan"]));
                    korisnik.SetLozinka(dr["Lozinka"].ToString());
                    korisnik.SetOIB(dr["OIB"].ToString());


                }
                MessageBox.Show(korisnik.GetIme_prezime());

                 BazaKontroler.Zatvori();
                return korisnik;
            }
            return null;
        }
        
    }
}
