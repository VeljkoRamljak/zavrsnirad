﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BikeSharing
{
    class Voznja
    {
        private int id_voznja;
        private DateTime vrijeme;
        private int id_korisnik;
        private int id_stanica;
        private int id_biciklo;
        private bool aktivna;

        public int GetId_voznja()
        {
            return id_voznja;
        }

        public void SetId_voznja(int value)
        {
            id_voznja = value;
        }

        public DateTime GetVrijeme()
        {
            return vrijeme;
        }

        public void SetVrijeme(DateTime value)
        {
            vrijeme = value;
        }

        public int GetId_korisnik()
        {
            return id_korisnik;
        }

        public void SetId_korisnik(int value)
        {
            id_korisnik = value;
        }

        public int GetId_stanica()
        {
            return id_stanica;
        }

        public void SetId_stanica(int value)
        {
            id_stanica = value;
        }

        public int GetId_biciklo()
        {
            return id_biciklo;
        }

        public void SetId_biciklo(int value)
        {
            id_biciklo = value;
        }

        public bool GetAktivna()
        {
            return aktivna;
        }

        public void SetAktivna(bool value)
        {
            aktivna = value;
        }
    }
}
