﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BikeSharing
{
    public partial class KorisnikVoznje : Form
    {
        public KorisnikVoznje()
        {
            InitializeComponent();
        }

        private void KorisnikVoznje_Load(object sender, EventArgs e)
        {
            String sql = "SELECT * FROM `voznja` WHERE `ID_korisnik` LIKE "+ KorisnikSesija.korSes.GetId_korisnik();
            dgvKorisnikVoznje.DataSource = BazaKontroler.IvediSqlVratiDT(sql);
        }
    }
}
