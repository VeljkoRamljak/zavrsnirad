﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BikeSharing
{
    public partial class Registracija : Form
    {
        public Registracija()
        {
            InitializeComponent();
        }

        private void btPosalji_Click(object sender, EventArgs e)
        {
            //validate string 
            
                string pattern = "[^a-žA-Ž]";
                if (Regex.IsMatch(tbImePrezime.Text, pattern))
                {
                tbImePrezime.Text = "";
                }
                
            
            //provjera jesu li upisani svi podaci
            if (string.IsNullOrEmpty(tbImePrezime.Text) || string.IsNullOrEmpty(tbEmail.Text)
                || string.IsNullOrEmpty(tbOIB.Text))
            {
                //ako nisu svi upisani
                MessageBox.Show("Molimo unesite sve podatke! ");
            }
            else //ako su svi upisani
            {
                //provjera jesu li ispravno(validno) upisani podaci
                long OIBProvjera;
                
                if ( !( Kontrola.IsValidEmail(tbEmail.Text) && Int64.TryParse(tbOIB.Text, out OIBProvjera) && Kontrola.CheckOIB(tbOIB.Text)  && !Kontrola.ProvjeraPostoliLiEmailUBazi(tbEmail.Text) ) )
                {
                    //ako nisu validno upisani
                    MessageBox.Show( Kontrola.IsValidEmail(tbEmail.Text).ToString()+ Int64.TryParse(tbOIB.Text, out OIBProvjera).ToString()+ Kontrola.CheckOIB(tbOIB.Text).ToString()+ (!Kontrola.ProvjeraPostoliLiEmailUBazi(tbEmail.Text)).ToString());
                    MessageBox.Show(OIBProvjera.ToString());
                    MessageBox.Show("Niste ispravno(validno) unijeli podatke!  oib je:" + Kontrola.CheckOIB(tbOIB.Text).ToString() + " email postoji u bazi:" + Kontrola.ProvjeraPostoliLiEmailUBazi(tbEmail.Text).ToString());
                }
                else
                {   //ako su validno upisani
                    //Izvedi sql tako da se povlače podaci iz text boxsova
                    
                   //Generate lozinka
                    int min = 0000;
                    int max = 9999;
                    Random rdm = new Random();
                    int lozinka = rdm.Next(min, max);

                    try
                    {
                        string mailBodyhtml =
                             "<p>You password "+lozinka.ToString()+" </p>";
                        var msg = new MailMessage("veljko.ramljak@gmail.com", tbEmail.Text, "DOBRODOŠLI", mailBodyhtml);
                        //msg.To.Add("to2@gmail.com");
                        msg.IsBodyHtml = true;
                        var smtpClient = new SmtpClient("smtp.gmail.com", 587); //**if your from email address is "from@hotmail.com" then host should be "smtp.hotmail.com"**
                        smtpClient.UseDefaultCredentials = true;
                        smtpClient.Credentials = new NetworkCredential("veljko.ramljak@gmail.com", "k-,.MION");
                        smtpClient.EnableSsl = true;
                        smtpClient.Send(msg);
                        MessageBox.Show("Email Sent Successfully Provjerite lozinku");

                        string sqlInserrt = "INSERT INTO `korisnik` (`ID_korisnik`, `Ime_Prezime`, `email`, `lozinka`, `admin`, `aktivan`, `OIB`) " +
                        " VALUES (NULL, '" + tbImePrezime.Text + "', '" + tbEmail.Text + "', '" + lozinka + "', '" + 0 + "', '" + 1 + "', '" + tbOIB.Text + "');";
                        MessageBox.Show(sqlInserrt);
                        BazaKontroler.InsertRow(sqlInserrt);
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        
                        MessageBox.Show("Provjerite vezu s internetom "+ex.ToString());
                    }

                    

                    //Uspjesno dodan novi korisnik
                    MessageBox.Show("Pregledajte mail");
                    
                }



            }

        }
    }
}
