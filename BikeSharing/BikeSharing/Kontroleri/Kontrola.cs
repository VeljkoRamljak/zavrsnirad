﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BikeSharing
{
    class Kontrola
    {
        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        public static bool CheckOIB(string oib)
        {
            if (oib.Length != 11) return false;

            long b;
            if (!long.TryParse(oib, out b)) return false;

            int a = 10;
            for (int i = 0; i < 10; i++)
            {
                a = a + Convert.ToInt32(oib.Substring(i, 1));
                a = a % 10;
                if (a == 0) a = 10;
                a *= 2;
                a = a % 11;
            }
            int control = 11 - a;
            if (control == 10) control = 0;

            return control == Convert.ToInt32(oib.Substring(10, 1));
        }

        public static bool ProvjeraPostoliLiEmailUBazi(string email)
        {
            string sql = "SELECT * FROM `korisnik` WHERE `email` like '%" + email + "%' ";
            try
            {
                     OdbcDataReader dr = BazaKontroler.IzvediSQL(sql);
                if(dr != null && true == dr.HasRows)
                { 
                    if (dr.Read())
                    {
                        if (dr["email"].ToString() == email)
                        {
                            BazaKontroler.Zatvori();
                            return true;
                        }
                        BazaKontroler.Zatvori();
                        return false;
                    }
                    BazaKontroler.Zatvori();
                    return false;
                }
                BazaKontroler.Zatvori();
                return false;

            }
            catch(Exception ex)
            {
                BazaKontroler.Zatvori();
                MessageBox.Show(ex.Message);
                return false;
            }
           
            
        }
    }
}
