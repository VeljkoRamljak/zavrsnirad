﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BikeSharing
{
    public partial class NovoBiciklo : Form
    {
        public NovoBiciklo()
        {
            InitializeComponent();
        }

       
        private void NovoBiciklo_Load(object sender, EventArgs e)
        {
            String sql = "SELECT * FROM `biciklo` WHERE 1 ";
            dgvBiciklo.DataSource = BazaKontroler.IvediSqlVratiDT(sql);

            //try>> ukoliko se ne odabere biciklo u odabir ulazi prvo s reda :)>>>
            try
            {

                int index = dgvBiciklo.CurrentCell.RowIndex;
                DataGridViewRow odabraniRed = dgvBiciklo.Rows[index];

                BikeSes.bikeSes.SetId_biciklo(Int32.Parse(odabraniRed.Cells["id_biciklo"].Value.ToString()));
                BikeSes.bikeSes.SetVelicina_guma(odabraniRed.Cells["velicina_guma"].Value.ToString());
                BikeSes.bikeSes.SetBroj_na_terminalu(Int32.Parse(odabraniRed.Cells["broj_na_terminalu"].Value.ToString()));
                BikeSes.bikeSes.SetId_stanica(Int32.Parse(odabraniRed.Cells["id_stanica"].Value.ToString()));
                BikeSes.bikeSes.SetModel(odabraniRed.Cells["model"].Value.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        //insert tipka
        private void btInsert_Click(object sender, EventArgs e)
        {

            //provjera jesu li upisani svi podaci
            if (string.IsNullOrEmpty(tbModel.Text) || string.IsNullOrEmpty(tbVelicinaGuma.Text)
                || string.IsNullOrEmpty(tbBrojNaTerminalu.Text) || string.IsNullOrEmpty(tbID_stanica.Text))
            {
                //ako nisu svi upisani
                MessageBox.Show("Molimo unesite sve podatke! ");
            }
            else //ako su svi upisani
            {
                //provjera jesu li ispravno(validno) upisani podaci
                int velicinaGumaProvjera;
                int brojNaTerminaluProvjera;
                int id_stanicaProvjera;
                
                if (!(Int32.TryParse(tbVelicinaGuma.Text, out velicinaGumaProvjera)
                    && Int32.TryParse(tbBrojNaTerminalu.Text, out brojNaTerminaluProvjera)
                    && Int32.TryParse(tbID_stanica.Text, out id_stanicaProvjera)))
                {
                    //ako nisu validno upisani
                    MessageBox.Show("Niste ispravno(validno) unijeli podatke!");
                }
                else
                {   //ako su validno upisani
                    //Izvedi sql tako da se povlače podaci iz text boxsova
                    string sqlInsta = "INSERT INTO `biciklo`(`ID_biciklo`, `model`, `velicina_guma`, `broj_na_terminalu`, `ID_stanica`)" +
                        " VALUES (NULL,'" + tbModel.Text + "',"+tbVelicinaGuma.Text+ "," + tbBrojNaTerminalu.Text + "," + tbID_stanica.Text + ")";
                    MessageBox.Show(sqlInsta);
                    BazaKontroler.InsertRow(sqlInsta);

                    //Uspjesno dodano novo biciklo
                    MessageBox.Show("Uspjesno dodano novo biciklo");
                    //refreš tablice
                    String sql = "SELECT * FROM `biciklo` WHERE 1 ";
                    dgvBiciklo.DataSource = BazaKontroler.IvediSqlVratiDT(sql);
                }
                
            }
            
        }
        //update tipka
        private void btUpdate_Click(object sender, EventArgs e)
        {
            //provjera jesu li upisani svi podaci
            if (string.IsNullOrEmpty(tbModel.Text) || string.IsNullOrEmpty(tbVelicinaGuma.Text)
                || string.IsNullOrEmpty(tbBrojNaTerminalu.Text) || string.IsNullOrEmpty(tbID_stanica.Text))
            {
                //ako nisu svi upisani
                MessageBox.Show("Molimo unesite sve podatke! ");
            }
            else //ako su svi upisani
            {
                //provjera jesu li ispravno(validno) upisani podaci
                int velicinaGumaProvjera;
                int brojNaTerminaluProvjera;
                int id_stanicaProvjera;

                if (!(Int32.TryParse(tbVelicinaGuma.Text, out velicinaGumaProvjera)
                    && Int32.TryParse(tbBrojNaTerminalu.Text, out brojNaTerminaluProvjera)
                    && Int32.TryParse(tbID_stanica.Text, out id_stanicaProvjera)))
                {
                    //ako nisu validno upisani
                    MessageBox.Show("Niste ispravno(validno) unijeli podatke!");
                }
                else
                {   //ako su validno upisani
                    //Izvedi sql update tako da se povlače podaci iz text boxsova
                    string sqlUpda = "UPDATE `biciklo` SET" +
                        " `model`='" + tbModel.Text + "',`velicina_guma`='" + tbVelicinaGuma.Text + "'," +
                        "`broj_na_terminalu`='" + tbBrojNaTerminalu.Text + "',`ID_stanica`='" + tbID_stanica.Text + "'" +
                        " WHERE `ID_biciklo`like "+BikeSes.bikeSes.GetId_biciklo()+"";
                  // MessageBox.Show(sqlUpda);
                    BazaKontroler.Update(sqlUpda);

                    //Uspjesno izmjenjeno  biciklo
                    MessageBox.Show("Uspjesno izmjenjeno biciklo");
                    //refreš tablice
                    String sql = "SELECT * FROM `biciklo` WHERE 1 ";
                    dgvBiciklo.DataSource = BazaKontroler.IvediSqlVratiDT(sql);
                }

            }
        }

        private void btDelete_Click(object sender, EventArgs e)
        {
            //Izbaciti prozor sigurnosti da li se ste sigurni
            DialogResult diaR = MessageBox.Show("Jeste li sigurni da želite izbrisati biciklo?",
                      "Upozorenje!!", MessageBoxButtons.YesNo);
            switch (diaR)
            {
                case DialogResult.Yes:
                    //DOKVATITI ID_biciklo od kliknutog bicikla
                    string sqlDelete = "DELETE FROM `biciklo` WHERE `ID_biciklo` = " + BikeSes.bikeSes.GetId_biciklo() + "";

                    MessageBox.Show(sqlDelete);

                    BazaKontroler.Delete(sqlDelete);

                    //Trajno uklonjen korisnik
                    MessageBox.Show("Trajno uklonjeno biciklo: " + BikeSes.bikeSes.GetModel());
                    //refreš tablice
                    String sql = "SELECT * FROM `biciklo` WHERE 1 ";
                    dgvBiciklo.DataSource = BazaKontroler.IvediSqlVratiDT(sql);
                    break;
                case DialogResult.No:
                    break;
            }
        }
        //na klik
        private void dgvBiciklo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int index = e.RowIndex;
                DataGridViewRow odabraniRed = dgvBiciklo.Rows[index];

                BikeSes.bikeSes.SetId_biciklo(Int32.Parse(odabraniRed.Cells["id_biciklo"].Value.ToString()));
                BikeSes.bikeSes.SetVelicina_guma(odabraniRed.Cells["velicina_guma"].Value.ToString());
                BikeSes.bikeSes.SetBroj_na_terminalu(Int32.Parse(odabraniRed.Cells["broj_na_terminalu"].Value.ToString()));
                BikeSes.bikeSes.SetId_stanica(Int32.Parse(odabraniRed.Cells["id_stanica"].Value.ToString()));
                BikeSes.bikeSes.SetModel(odabraniRed.Cells["model"].Value.ToString());

                tbModel.Text = BikeSes.bikeSes.GetModel();
                tbVelicinaGuma.Text = BikeSes.bikeSes.GetVelicina_guma();
                tbBrojNaTerminalu.Text = BikeSes.bikeSes.GetBroj_na_terminalu().ToString();
                tbID_stanica.Text = BikeSes.bikeSes.GetId_stanica().ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        
    }
}
