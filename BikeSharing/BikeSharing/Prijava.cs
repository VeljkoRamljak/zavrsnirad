﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Odbc;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BikeSharing
{
    public partial class Prijava : Form
    {
        //private BazaKontroler baza;
        public Prijava()
        {
            InitializeComponent();
        }


        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Prijava_Load(object sender, EventArgs e)
        {
            
            MessageBox.Show("Ovo je u prijavi on load");
           
            
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String email = textBox1.Text;
            int lozz=0;

            if (!Int32.TryParse(textBox2.Text, out lozz) || !Kontrola.IsValidEmail(email))
                MessageBox.Show("lozinka se sastoji od 4 broja a email: primjer@email.hr ", "Pogrešan format unosa", MessageBoxButtons.OK, MessageBoxIcon.Error);


            else
            {
                MessageBox.Show("validacija je prošla");
                String sql = "SELECT * FROM `korisnik` WHERE `email` like '%" + email + "%' AND `lozinka` like " + lozz+" AND `aktivan` = 1";
                
                KorisnikSesija.korSes = Korisnik.DohvatiKorisnikaIzBaze(BazaKontroler.IzvediSQL(sql));
                BazaKontroler.Zatvori();
                
                try
                {
                   
                    
                    string sql1 = "SELECT * FROM `voznja` WHERE `ID_korisnik` = " + KorisnikSesija.korSes.GetId_korisnik().ToString() + " AND `aktivna` = 1";
                    
                    
                    
                    OdbcDataReader drr = BazaKontroler.IzvediSQL(sql1);
                    
                    
                    if (drr != null && drr.HasRows == true)
                    {
                        while (drr.Read())
                        {
                            VoznjaSes.vozSes.SetId_voznja(Int32.Parse(drr["ID_voznja"].ToString()));
                            VoznjaSes.vozSes.SetId_biciklo(Int32.Parse(drr["ID_biciklo"].ToString()));
                        }


                        string sqql = "SELECT TIMESTAMPDIFF(minute, `vrijeme_pocetka`, CURRENT_TIME()) AS minuta FROM `voznja` WHERE `aktivna` like 1 AND `ID_korisnik` LIKE " + KorisnikSesija.korSes.GetId_korisnik() + " ";
                        OdbcDataReader dataR = BazaKontroler.IzvediSQL(sqql);
                        dataR.Read();
                        int minn=0;
                        Int32.TryParse(dataR["minuta"].ToString(),out minn);
                        if (Math.Abs(minn) > 30)
                        {
                            MessageBox.Show("vozili ste više od pola sata");
                        }
                        //ako postoji update na njoj iz aktivne u neaktivnu, te upisati vrijeme završetka, 
                        
                        string sql2 = "UPDATE `voznja` SET `aktivna` = 0, `vrijeme_zavrsetka` = CURRENT_TIME(), `ID_stanica_zavrsetka` = "+StanicaSesija.stanSes.GetId_stanica().ToString()+" WHERE `ID_voznja` LIKE " + VoznjaSes.vozSes.GetId_voznja() + ";";
                        
                        BazaKontroler.Update(sql2);
                        //NEKI IF ELSE BESPLANO<30min>morate platiti


                        //biciklo vratiti update i broj stanice
                        string sql3 = "UPDATE `biciklo` SET `ID_stanica` = "+StanicaSesija.stanSes.GetId_stanica()+" WHERE `ID_biciklo` LIKE " + VoznjaSes.vozSes.GetId_biciklo() + ";";
                        MessageBox.Show(sql3);
                        BazaKontroler.Update(sql3);

                        ////hvala lijepa ispistati i vratiti na log in
                        MessageBox.Show("hvala lijepa do sljedeće vožnje ćao");
                    }
                    

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                    try
                    {
                    if (String.IsNullOrEmpty(KorisnikSesija.korSes.GetId_korisnik()))
                    {
                        MessageBox.Show("Pogrešni podaci, molimo pokušajte ponovo");
                    }
                }
                catch(Exception ex) {
                    MessageBox.Show("Pogrešni podaci, molimo pokušajte ponovo");
                }
                
                try
                {
                    if (!String.IsNullOrEmpty(KorisnikSesija.korSes.GetId_korisnik()))
                    {
                        
                        if (KorisnikSesija.korSes.GetAdmin())
                        {
                            PocetnaAdmin pocA = new PocetnaAdmin();
                            pocA.Show();
                            this.Hide();

                        }
                        else
                        {
                            Pocetna poc = new Pocetna(); // Instantiate a Form3 object.
                            poc.Show(); // Show Form3 and
                            this.Hide();
                        }
                                
                    }
                }
                catch (Exception ex)
                {
                    
                    MessageBox.Show("eksepšion provjere ukoliko" +
                        " postoji sesija korisnika"+ ex.StackTrace.ToString());
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Registracija reg = new Registracija(); // Instantiate a Form3 object.
            reg.Show(); // Show Form3 and
            
        }
    }
}
