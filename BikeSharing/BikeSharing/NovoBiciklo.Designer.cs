﻿namespace BikeSharing
{
    partial class NovoBiciklo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvBiciklo = new System.Windows.Forms.DataGridView();
            this.btInsert = new System.Windows.Forms.Button();
            this.btUpdate = new System.Windows.Forms.Button();
            this.btDelete = new System.Windows.Forms.Button();
            this.tbModel = new System.Windows.Forms.TextBox();
            this.tbVelicinaGuma = new System.Windows.Forms.TextBox();
            this.tbBrojNaTerminalu = new System.Windows.Forms.TextBox();
            this.tbID_stanica = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBiciklo)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvBiciklo
            // 
            this.dgvBiciklo.AllowUserToAddRows = false;
            this.dgvBiciklo.AllowUserToDeleteRows = false;
            this.dgvBiciklo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBiciklo.Location = new System.Drawing.Point(13, 72);
            this.dgvBiciklo.Name = "dgvBiciklo";
            this.dgvBiciklo.ReadOnly = true;
            this.dgvBiciklo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBiciklo.Size = new System.Drawing.Size(545, 426);
            this.dgvBiciklo.TabIndex = 0;
            this.dgvBiciklo.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBiciklo_CellClick);
            // 
            // btInsert
            // 
            this.btInsert.Location = new System.Drawing.Point(748, 234);
            this.btInsert.Name = "btInsert";
            this.btInsert.Size = new System.Drawing.Size(112, 70);
            this.btInsert.TabIndex = 1;
            this.btInsert.Text = "Insert";
            this.btInsert.UseVisualStyleBackColor = true;
            this.btInsert.Click += new System.EventHandler(this.btInsert_Click);
            // 
            // btUpdate
            // 
            this.btUpdate.Location = new System.Drawing.Point(770, 310);
            this.btUpdate.Name = "btUpdate";
            this.btUpdate.Size = new System.Drawing.Size(70, 112);
            this.btUpdate.TabIndex = 2;
            this.btUpdate.Text = "Update";
            this.btUpdate.UseVisualStyleBackColor = true;
            this.btUpdate.Click += new System.EventHandler(this.btUpdate_Click);
            // 
            // btDelete
            // 
            this.btDelete.Location = new System.Drawing.Point(748, 428);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(112, 70);
            this.btDelete.TabIndex = 3;
            this.btDelete.Text = "Delete";
            this.btDelete.UseVisualStyleBackColor = true;
            this.btDelete.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // tbModel
            // 
            this.tbModel.Location = new System.Drawing.Point(144, 46);
            this.tbModel.Name = "tbModel";
            this.tbModel.Size = new System.Drawing.Size(93, 20);
            this.tbModel.TabIndex = 4;
            // 
            // tbVelicinaGuma
            // 
            this.tbVelicinaGuma.Location = new System.Drawing.Point(251, 46);
            this.tbVelicinaGuma.Name = "tbVelicinaGuma";
            this.tbVelicinaGuma.Size = new System.Drawing.Size(93, 20);
            this.tbVelicinaGuma.TabIndex = 5;
            // 
            // tbBrojNaTerminalu
            // 
            this.tbBrojNaTerminalu.Location = new System.Drawing.Point(358, 46);
            this.tbBrojNaTerminalu.Name = "tbBrojNaTerminalu";
            this.tbBrojNaTerminalu.Size = new System.Drawing.Size(93, 20);
            this.tbBrojNaTerminalu.TabIndex = 6;
            // 
            // tbID_stanica
            // 
            this.tbID_stanica.Location = new System.Drawing.Point(465, 46);
            this.tbID_stanica.Name = "tbID_stanica";
            this.tbID_stanica.Size = new System.Drawing.Size(93, 20);
            this.tbID_stanica.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(141, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Model";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(248, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "velicina_guma";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(355, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "broj_na_terminalu";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(462, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "ID_stanica";
            // 
            // NovoBiciklo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 556);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbID_stanica);
            this.Controls.Add(this.tbBrojNaTerminalu);
            this.Controls.Add(this.tbVelicinaGuma);
            this.Controls.Add(this.tbModel);
            this.Controls.Add(this.btDelete);
            this.Controls.Add(this.btUpdate);
            this.Controls.Add(this.btInsert);
            this.Controls.Add(this.dgvBiciklo);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "NovoBiciklo";
            this.Text = "Novo Biciklo";
            this.Load += new System.EventHandler(this.NovoBiciklo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBiciklo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvBiciklo;
        private System.Windows.Forms.Button btInsert;
        private System.Windows.Forms.Button btUpdate;
        private System.Windows.Forms.Button btDelete;
        private System.Windows.Forms.TextBox tbModel;
        private System.Windows.Forms.TextBox tbVelicinaGuma;
        private System.Windows.Forms.TextBox tbBrojNaTerminalu;
        private System.Windows.Forms.TextBox tbID_stanica;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}