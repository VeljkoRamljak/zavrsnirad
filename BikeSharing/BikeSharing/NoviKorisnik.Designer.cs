﻿namespace BikeSharing
{
    partial class NoviKorisnik
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbImePrezime = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btInsert = new System.Windows.Forms.Button();
            this.tbLozinka = new System.Windows.Forms.TextBox();
            this.tbAdmin = new System.Windows.Forms.TextBox();
            this.tbAktivan = new System.Windows.Forms.TextBox();
            this.tbOIB = new System.Windows.Forms.TextBox();
            this.tbEmail = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dgvKorisnik = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.btDelete = new System.Windows.Forms.Button();
            this.btPregledajKorisnika = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKorisnik)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(151, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ime i prezime";
            // 
            // tbImePrezime
            // 
            this.tbImePrezime.Location = new System.Drawing.Point(154, 36);
            this.tbImePrezime.Name = "tbImePrezime";
            this.tbImePrezime.Size = new System.Drawing.Size(116, 20);
            this.tbImePrezime.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(273, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "email";
            // 
            // btInsert
            // 
            this.btInsert.Location = new System.Drawing.Point(882, 280);
            this.btInsert.Name = "btInsert";
            this.btInsert.Size = new System.Drawing.Size(114, 58);
            this.btInsert.TabIndex = 4;
            this.btInsert.Text = "Insert";
            this.btInsert.UseVisualStyleBackColor = true;
            this.btInsert.Click += new System.EventHandler(this.button1_Click);
            // 
            // tbLozinka
            // 
            this.tbLozinka.Location = new System.Drawing.Point(395, 36);
            this.tbLozinka.Name = "tbLozinka";
            this.tbLozinka.Size = new System.Drawing.Size(118, 20);
            this.tbLozinka.TabIndex = 2;
            // 
            // tbAdmin
            // 
            this.tbAdmin.Location = new System.Drawing.Point(519, 36);
            this.tbAdmin.Name = "tbAdmin";
            this.tbAdmin.Size = new System.Drawing.Size(62, 20);
            this.tbAdmin.TabIndex = 2;
            // 
            // tbAktivan
            // 
            this.tbAktivan.Location = new System.Drawing.Point(587, 36);
            this.tbAktivan.Name = "tbAktivan";
            this.tbAktivan.Size = new System.Drawing.Size(63, 20);
            this.tbAktivan.TabIndex = 2;
            // 
            // tbOIB
            // 
            this.tbOIB.Location = new System.Drawing.Point(656, 37);
            this.tbOIB.Name = "tbOIB";
            this.tbOIB.Size = new System.Drawing.Size(92, 20);
            this.tbOIB.TabIndex = 2;
            // 
            // tbEmail
            // 
            this.tbEmail.Location = new System.Drawing.Point(276, 36);
            this.tbEmail.Name = "tbEmail";
            this.tbEmail.Size = new System.Drawing.Size(113, 20);
            this.tbEmail.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(392, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "lozinka";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(516, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "admin";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(584, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "aktivan";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(748, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 13);
            this.label6.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(656, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "OIB";
            // 
            // dgvKorisnik
            // 
            this.dgvKorisnik.AllowUserToAddRows = false;
            this.dgvKorisnik.AllowUserToDeleteRows = false;
            this.dgvKorisnik.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvKorisnik.Location = new System.Drawing.Point(13, 63);
            this.dgvKorisnik.Name = "dgvKorisnik";
            this.dgvKorisnik.ReadOnly = true;
            this.dgvKorisnik.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvKorisnik.Size = new System.Drawing.Size(735, 521);
            this.dgvKorisnik.TabIndex = 5;
            this.dgvKorisnik.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvKorisnik_CellClick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(932, 344);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(64, 90);
            this.button1.TabIndex = 6;
            this.button1.Text = "Update";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btDelete
            // 
            this.btDelete.Location = new System.Drawing.Point(882, 440);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(114, 58);
            this.btDelete.TabIndex = 8;
            this.btDelete.Text = "Delete";
            this.btDelete.UseVisualStyleBackColor = true;
            this.btDelete.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // btPregledajKorisnika
            // 
            this.btPregledajKorisnika.Location = new System.Drawing.Point(867, 63);
            this.btPregledajKorisnika.Name = "btPregledajKorisnika";
            this.btPregledajKorisnika.Size = new System.Drawing.Size(114, 58);
            this.btPregledajKorisnika.TabIndex = 9;
            this.btPregledajKorisnika.Text = "Pregledaj korisnika";
            this.btPregledajKorisnika.UseVisualStyleBackColor = true;
            this.btPregledajKorisnika.Click += new System.EventHandler(this.btPregledajKorisnika_Click);
            // 
            // NoviKorisnik
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1090, 596);
            this.Controls.Add(this.btPregledajKorisnika);
            this.Controls.Add(this.btDelete);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dgvKorisnik);
            this.Controls.Add(this.btInsert);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbOIB);
            this.Controls.Add(this.tbAktivan);
            this.Controls.Add(this.tbAdmin);
            this.Controls.Add(this.tbLozinka);
            this.Controls.Add(this.tbEmail);
            this.Controls.Add(this.tbImePrezime);
            this.Controls.Add(this.label1);
            this.Name = "NoviKorisnik";
            this.Text = "Novi korisnik";
            this.Load += new System.EventHandler(this.Form4_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvKorisnik)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbImePrezime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btInsert;
        private System.Windows.Forms.TextBox tbLozinka;
        private System.Windows.Forms.TextBox tbAdmin;
        private System.Windows.Forms.TextBox tbAktivan;
        private System.Windows.Forms.TextBox tbOIB;
        private System.Windows.Forms.TextBox tbEmail;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dgvKorisnik;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btDelete;
        private System.Windows.Forms.Button btPregledajKorisnika;
    }
}